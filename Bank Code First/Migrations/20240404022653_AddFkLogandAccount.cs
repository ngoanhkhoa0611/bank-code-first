﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bank_Code_First.Migrations
{
    /// <inheritdoc />
    public partial class AddFkLogandAccount : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountId",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LogId",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ReportName",
                table: "Reports",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountId",
                table: "Reports",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_LogId",
                table: "Reports",
                column: "LogId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "AccountId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports",
                column: "LogId",
                principalTable: "Logs",
                principalColumn: "LogID",
                onDelete: ReferentialAction.NoAction);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_AccountId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_LogId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "LogId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "ReportName",
                table: "Reports");
        }
    }
}
