﻿using System.ComponentModel.DataAnnotations;

namespace Bank_Code_First.Models
{
    public class Log
    {
        [Key]
        public int LogID { get; set; }
        public DateTime LogDate { get; set; }
        public TimeSpan LoginTime { get; set; }

        public int TransId { get; set; }

        public Transaction Transaction { get; set; }

        public ICollection<Report>  Reports { get; set; }
    }
}
