﻿using System.ComponentModel.DataAnnotations;

namespace Bank_Code_First.Models
{
    public class Report
    {
        [Key]
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }

        public int TransId { get; set; }
        public int LogId { get; set; }
        public int AccountId { get; set; }


        public Transaction Transaction { get; set; }
        public Log Log { get; set; }
        public Account Account { get; set; }
    }
}
