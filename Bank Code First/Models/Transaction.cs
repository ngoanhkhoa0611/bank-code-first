﻿using System.ComponentModel.DataAnnotations;

namespace Bank_Code_First.Models
{
    public class Transaction
    {
        [Key]
        public int TransId { get; set; }
        public string Name { get; set; }

        public int CustomerID { get; set; }
        public int EmployeeId { get; set; }

        public Customer Customer { get; set; }

        public ICollection<Report> Reports { get; set; }
        public ICollection<Log> Logs { get; set; }

    }
}
