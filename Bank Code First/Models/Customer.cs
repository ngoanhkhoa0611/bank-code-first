﻿using System.ComponentModel.DataAnnotations;

namespace Bank_Code_First.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}
