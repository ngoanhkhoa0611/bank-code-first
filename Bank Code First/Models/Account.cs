﻿using System.ComponentModel.DataAnnotations;

namespace Bank_Code_First.Models
{
    public class Account
    {
        [Key]
        public int AccountId { get; set; }
        public string AccountName { get; set; }

        public int CustomerID { get; set; }

        public Customer Customer { get; set; }

        public ICollection<Report> Reports { get; set; }
    }
}
