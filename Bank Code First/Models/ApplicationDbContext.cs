﻿using Microsoft.EntityFrameworkCore;

namespace Bank_Code_First.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Report>()
                .HasOne(r => r.Transaction)
                .WithMany(t => t.Reports)
                .HasForeignKey(r => r.TransId);

            modelBuilder.Entity<Log>()
                .HasOne(r => r.Transaction)
                .WithMany(t => t.Logs)
                .HasForeignKey(r => r.TransId);

            modelBuilder.Entity<Report>()
                .HasOne(r => r.Log)
                .WithMany(t => t.Reports)
                .HasForeignKey(r => r.LogId);
        }
    }
}
